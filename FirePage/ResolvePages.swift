//
//  ResolvePages.swift
//  FirePage
//
//  Created by The Ritler on 12/29/16.
//  Copyright © 2016 The Ritler. All rights reserved.
//
import UIKit
import Firebase
import Foundation

class ResolvePages: UIViewController, UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate {
    // Accesing global variables through APP DELEGATE
    
     let master = UIApplication.shared.delegate as! AppDelegate
    
    
    // Accesing Firebase
    
    let pageRef = FIRDatabase.database().reference().child("pages").child("Ritwik")
    
    // Dynamic Scaling Functions
    
    func DFrame (oldFrame: CGRect) -> CGRect {
        let newX = Int((Float(oldFrame.minX)/414.0)*Float(UIScreen.main.bounds.width))
        let newY = Int((Float(oldFrame.minY)/736.0)*Float(UIScreen.main.bounds.height))
        let newW = Int((Float(oldFrame.width)/414.0)*Float(UIScreen.main.bounds.width))
        let newH = Int((Float(oldFrame.height)/736.0)*Float(UIScreen.main.bounds.height))
        
        return CGRect(x: newX, y: newY, width: newW, height: newH)
    }
    func DPoint (oldPoint: CGPoint) -> CGPoint {
        let newX = Int((Float(oldPoint.x)/414.0)*Float(UIScreen.main.bounds.width))
        let newY = Int((Float(oldPoint.y)/736.0)*Float(UIScreen.main.bounds.height))
        return CGPoint(x: newX, y: newY)
    }
    
    // Set view bound paramters relative to iphone 7+
    
    let viewX = 414
    let viewY = 736
    
    
    
    // Initialize scrollview
    
    let scrollView = UIScrollView(frame: UIScreen.main.bounds)
    
    // TableView Elements
    
    var isRowSelected = false
    var selectedRow = String()
    
    // TableView Data
    
    var tableView: UITableView  =   UITableView()
    var unresolved = [String]()
    var helpRequests = [String]()
    var info = [String: [String : String]]()
    
    // Entering and Storing Data
    
    var currentPageStatuses = [String: [String : String]]()
    var alertState = false
    let monthsOfTheYear:[String] = ["January", "February","March", "April", "May", "June", "July", "August", "September", "October", "Novemeber", "December"]
    
    // Set Table View Properties
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return helpRequests.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // Set backfround to gray for normal entries
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor(red: CGFloat(210.0/225.0), green: CGFloat(210.0/225.0), blue: CGFloat(210.0/225.0), alpha: 0.9)
        cell.textLabel!.text = daytoDate[helpRequests [indexPath.row]]
        
        // set background to red for unresolved entries
        if unresolved.contains(helpRequests[indexPath.row]) {
            cell.backgroundColor = UIColor(red: CGFloat(178.0/225.0), green: CGFloat(34.0/225.0), blue: CGFloat(34.0/225.0), alpha: 0.9)
            bgColorView.backgroundColor = UIColor(red: CGFloat(151.0/225.0), green: CGFloat(22.0/225.0), blue: CGFloat(22.0/225.0), alpha: 0.9)
            
        }else{
            
        }
        cell.selectedBackgroundView = bgColorView
        return cell;
    }
    func connected(sender: UIButton!) {
        
        //print("connection successful")
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        // print(indexPath)
        // print("mememme")
        let a : String = info[helpRequests[indexPath.row]]!["name"]!
        let b : String = info[helpRequests[indexPath.row]]!["location"]!
        let c : String = info[helpRequests[indexPath.row]]!["description"]!
        
        //Show current page
        
        name.text = "      Time: " + helpRequests[indexPath.row]
        number.text = "     Name: " + a
        loc.text = "Location: " + b
        desc.text = " Problem: " + c
        selectedRow = helpRequests[indexPath.row]
        // print(info[helpRequests[indexPath.row]])
        isRowSelected = true
    }
    
    
    // Initialization of Labels, Buttons, and
    
    var descriptionField = UITextField()
    var name = UILabel(frame: CGRect(x: 0, y: 200, width: 300, height: 50))
    var number = UILabel(frame: CGRect(x: 0, y: 200, width: 300, height: 50))
    var desc = UILabel(frame: CGRect(x: 0, y: 200, width: 300, height: 50))
    var loc = UILabel(frame: CGRect(x: 0, y: 200, width: 300, height: 50))
    var resolveButton = UIButton(frame: CGRect(x: 0, y: 200, width: 100, height: 50))
    
    
    // Calendar Elements
    
    var date = NSDate()
    var calendar = NSCalendar.current
    var month = 1
    var year = 1
    var day = 1
    var hour = 1
    var minute = 1
    var second = 1
    var daytoDate = [String: String]()
    
    
    
    
    // Initialize storage for Popup elements and set clear function
    
    var popupElements = [UIView]()
    
    func exitButton(sendero: UIButton!) {
        clearPopup()
    }
    
    // Creates and fills Popup
    
    func createPopup(){
        
        // Intialize Frame and Popup Background and set Attrubutes
        
        let PopupFrame = DFrame (oldFrame: CGRect(x: viewX/2 , y: viewY/2, width: viewX - 100, height: viewY - 200 ))
        let Popup = UIView(frame: PopupFrame)
        Popup.center = DPoint (oldPoint: CGPoint(x: viewX/2 , y: viewY/2))
        Popup.backgroundColor = .white
        Popup.layer.cornerRadius = 10
        Popup.layer.shadowRadius = 10.0;
        Popup.layer.shadowOpacity = 0.5;
        
        
        // Intitialize Popuop escapoe button and set Attrubutes
        
        let PopupX = UIButton(frame: DFrame(oldFrame: CGRect(x : 0, y: 0, width: 20, height: 20)))
        PopupX.center = DPoint (oldPoint: CGPoint(x: viewX - 65, y : 115 ))
        PopupX.setImage(#imageLiteral(resourceName: "Close"), for: .normal)
        PopupX.addTarget(self, action: #selector(exitButton), for: .touchUpInside)
        
        
        
        // Initialize Description Field and Set attrubutes
        
        descriptionField = UITextField(frame: CGRect(x: 20, y: 100, width: 260, height: 50))
        descriptionField.center = DPoint (oldPoint: CGPoint(x: viewX/2, y: 380))
        descriptionField.placeholder = "How did you resolve it"
        descriptionField.font = UIFont.systemFont(ofSize: 15)
        descriptionField.borderStyle = UITextBorderStyle.roundedRect
        descriptionField.autocorrectionType = UITextAutocorrectionType.no
        // locationField.backgroundColor = .red
        descriptionField.keyboardType = UIKeyboardType.default
        descriptionField.returnKeyType = UIReturnKeyType.done
        descriptionField.clearButtonMode = UITextFieldViewMode.whileEditing;
        descriptionField.contentVerticalAlignment =         UIControlContentVerticalAlignment.center
        if info[selectedRow]!["resolution"] != nil  {
            descriptionField.text = info[selectedRow]!["resolution"]
        }
        
        descriptionField.delegate = self
        
        
        // Initialize enter Button and Set Attibutes
        
        let enterButton = UIButton(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 100, height: 50)))
        enterButton.center = DPoint (oldPoint: CGPoint(x: viewX/2 , y: (viewY/2) + 130 ))
        enterButton.backgroundColor = UIColor(red: CGFloat(102.0/225.0), green: CGFloat(204.0/225.0), blue: CGFloat(0.0/225.0), alpha: 1.0)
        enterButton.setTitle("Enter", for: .normal)
        enterButton.layer.cornerRadius = 10
        enterButton.addTarget(self, action: #selector(enterAction), for: .touchUpInside)
        
        // Create Blur for when Popup is created
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        //always fill the view
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Create and set Attrubutes for the Name Label
        
        let nameInfo = UILabel(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 260, height: 48)))
        nameInfo.backgroundColor = .clear
        // nameInfo.layer.cornerRadius = 10
        nameInfo.center = self.DPoint (oldPoint: CGPoint(x: viewX/2, y: 160 ))
        nameInfo.text = name.text?.trim()
        nameInfo.clipsToBounds = true
        nameInfo.layer.cornerRadius = 5
        
        // Create and set Attrubutes for the Number Label
        
        let numberInfo = UILabel(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 260, height: 48)))
        numberInfo.backgroundColor = .clear
        numberInfo.layer.cornerRadius = 10
        numberInfo.center = self.DPoint (oldPoint: CGPoint(x: viewX/2, y: 210))
        numberInfo.text = number.text?.trim()
        numberInfo.clipsToBounds = true
        numberInfo.layer.cornerRadius = 5
        
        // Create and set Attrubutes for the Description Label
        
        let descInfo = UILabel(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 260, height: 48)))
        descInfo.backgroundColor = .clear
        descInfo.layer.cornerRadius = 10
        descInfo.center = self.DPoint (oldPoint: CGPoint(x: viewX/2, y: 260))
        descInfo.text = desc.text?.trim()
        descInfo.clipsToBounds = true
        descInfo.layer.cornerRadius = 5
        
        // Create and set Attrubutes for the Location Label
        
        let locInfo = UILabel(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 260, height: 48)))
        locInfo.backgroundColor = .clear
        locInfo.layer.cornerRadius = 10
        locInfo.center = self.DPoint (oldPoint: CGPoint(x: viewX/2, y: 310))
        locInfo.text = loc.text?.trim()
        locInfo.clipsToBounds = true
        locInfo.layer.cornerRadius = 5
        
        
        // Adds all Popup elements to the popupArray
        
        popupElements.append(descriptionField)
        popupElements.append(Popup)
        popupElements.append(PopupX)
        popupElements.append(enterButton)
        popupElements.append(nameInfo)
        popupElements.append(numberInfo)
        popupElements.append(descInfo)
        popupElements.append(locInfo)
        popupElements.append(blurEffectView)
        
        // Adds all the Popup elemtns to the View
        
        self.view.addSubview(blurEffectView)
        self.view.addSubview(PopupX)
        self.view.addSubview(Popup)
        self.view.bringSubview(toFront: PopupX)
        self.view.addSubview(enterButton)
        self.view.bringSubview(toFront: enterButton)
        self.view.addSubview(descriptionField)
        self.view.addSubview(nameInfo)
        self.view.addSubview(numberInfo)
        self.view.addSubview(descInfo)
        self.view.addSubview(locInfo)
        
    }

    
    // Action for Clicking enter on a page
    
    func enterAction(sendero: UIButton!) {
        // Forces the user(RA) to enter a resolutiob
        if descriptionField.text != "" {
        // Gets the help request that was chosen
        if let index = unresolved.index(of: selectedRow) {
            unresolved.remove(at: index)
        }
        // Resolves the help request
        info[selectedRow]!["status"] = "resolved"
        info[selectedRow]!["resolution"] = descriptionField.text
        
        // Reupdates the Datavase
        // pageRef.setValue(self.info)
        pageRef.updateChildValues(self.info)
        descriptionField.text = ""
        // helpRequests.removeAll()
    
        // Updates the tableview with new color
        tableView.reloadData()
        
        // removes the popup
        clearPopup()
        } else {
            descriptionField.attributedPlaceholder = NSAttributedString(string: "Please enter a resolution",
                                                              attributes: [NSForegroundColorAttributeName: UIColor.red])
            
        }
    }
    
    // Clears popup
    
    func clearPopup(){
        for element in popupElements {
            element.removeFromSuperview()
        }
    }
    
    
    // Resolve Button Action
    
    // var scrollView: UIScrollView!
    func resolveAction(sendero: UIButton!) {
        if isRowSelected{
        
        // myView.backgroundColor = .black
        //myView.center = DPoint (oldPoint: CGPoint(x: 250, y: 225)
        //self.view.addSubview(myView)
        //self.view.bringSubview(toFront: myView)
        createPopup()
        
        
        
        // tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        }
    }
    
    // Create Alert Action (unused atm)
    func alertAction(sender: SpecialButton!) {
        self.alertState = false
        self.currentPageStatuses[sender.hiddenText1]!["status"] = "resolved"
        
        pageRef.updateChildValues(self.currentPageStatuses)
        sender.removeFromSuperview()
        
        
        
    }
    
    // Alert function
    func alert (dict : [String: [String: String]]){
        for (time, info) in dict {
            if info["status"] == "unresolved"{
                // print("hehehe")
                self.alertState = true
                let btn = SpecialButton(frame: DFrame (oldFrame: CGRect(x: 50, y: 550, width: 200, height: 200)))
                btn.backgroundColor = .purple
                btn.center = DPoint (oldPoint: CGPoint(x: 210, y: 550))
                let dateComponents : [String] = time.components(separatedBy: "|")
                // print(dateComponents)
                var text = String()
                let monthText : String! = monthsOfTheYear[Int(dateComponents[0])! - 1]
                let dayText : String! = dateComponents[1]
                let yearText : String! = dateComponents[2]
                let hourText : String! = dateComponents[3]
                let minuteText : String! = dateComponents[4]
                let locationText : String = info["location"]!
                let descriptionText : String! = info["description"]!
                text = "Day: " + monthText + " " + dayText + ", " + yearText + "\nHour: " + hourText + ":" + minuteText + "\nLocation: " + locationText + "\nDescription: " + descriptionText + "\nName: " + master.name
                
                btn.hiddenText1 = time
                btn.titleLabel?.numberOfLines = 0
                
                
                btn.setTitle(text, for: .normal)
                btn.addTarget(self, action: #selector(alertAction), for: .touchUpInside)
                btn.titleLabel!.textAlignment = .center
                
                btn.layer.cornerRadius = 50
                btn.setTitleColor(.black , for: .normal)
                
                self.view.addSubview(btn)
                /*
                 while true {
                 if(alertState == false){
                 break
                 }else{
                 continue
                 }
                 }
                 */
                
                
            }
            
            
        }
        self.currentPageStatuses = dict
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create Gray Color
        
        // let grayColor = UIColor(red: CGFloat(220.0/225.0), green: CGFloat(220.0/225.0), blue: CGFloat(220.0/225.0), alpha: 0.9)
        
        // Intialize Dynamically sized Labels and Set Attributes
        
        name = UILabel(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 400, height: 48)))
        name.backgroundColor = .clear
        name.layer.cornerRadius = 10
        
        number = UILabel(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 400, height: 48)))
        number.backgroundColor = .clear
        number.layer.cornerRadius = 10
        
        desc = UILabel(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 400, height: 48)))
        desc.backgroundColor = .clear
        desc.layer.cornerRadius = 10
        
        loc = UILabel(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 400, height: 48)))
        loc.backgroundColor = .clear
        loc.layer.cornerRadius = 10
        
        // Intialize Resolve Button
        
        var resolveButton = UIButton(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 100, height: 50)))
        
        // Update Calendar Elements
        self.view.backgroundColor = .white
        self.calendar = NSCalendar.current
        self.month = calendar.component(.month, from: self.date as Date)
        self.year = calendar.component(.year, from: self.date as Date)
        self.day = calendar.component(.day, from: self.date as Date)
        self.hour = calendar.component(.hour, from: self.date as Date)
        self.minute = calendar.component(.minute, from: self.date as Date)
        self.second = calendar.component(.second, from: self.date as Date)
        
        // Create Usable Calendar Strings
        var month = String(format: "%02d", self.month)
        var day = String(format: "%02d", self.day)
        var year = String(format: "%04d", self.year)
        var hour = String(format: "%02d", self.hour)
        var minute = String(format: "%02d", self.minute)
        var second = String(format: "%02d", self.second)
        self.view.backgroundColor = UIColor(red: CGFloat(220.0/225.0), green: CGFloat(220.0/225.0), blue: CGFloat(220.0/225.0), alpha: 0.9)
        
        // Do any additional setup after loading the view, typically from a nib.
        
        //Get tableview elements from FireBase
        
        pageRef.observe(.value) { (snapshot: FIRDataSnapshot ) in
            var value : [String: [String : String]] = (snapshot.value as! NSDictionary) as! [String : [String : String]]
            // print(value)
            
            // Delete any leftover and unresolved Help Requests
            self.helpRequests.removeAll()
            self.unresolved.removeAll()
            
            // Go through dictionary of values from fireBase and store them in usable form
            for (time, data) in value {
                /*
                if data["status"] == "unresolved"{
                    self.unresolved.append(time)
                }
                self.helpRequests.append(time)
 */
                if data["name"] ==  nil {
                    value.removeValue(forKey: time)
                    // print("deleteo")
                }else{
                    var dateComponents : [String] = time.components(separatedBy: "|")
                    var monthText : String! = dateComponents[0]
                    var dayText : String! = dateComponents[1]
                    var yearText : String! = dateComponents[2]
                    var hourText : String! = dateComponents[3]
                    var minuteText : String! = dateComponents[4]
                    if monthText == month && yearText == year && dayText == day {
                        // print("foundo")
                        let montho : String! = self.monthsOfTheYear[Int(dateComponents[0])! - 1]
                        let englishDate = montho + " " + dayText + ", " + yearText + " " + hourText + ":" + minuteText
                        self.daytoDate[time] = englishDate
                    if data["status"] == "unresolved"{
                        self.unresolved.append(time)
                    }
                    
                    self.helpRequests.append(time)
                    }
                }
                
            }
            
            // Sort the list by Date (we want latest first so we reverse)
            
            self.helpRequests.sort()
            self.helpRequests.reverse()
            // print(self.daytoDate)
            
            // Set the dictionary and take out any bad entries
            
            self.info = value
            for (time, data) in value {
                
                if data["name"] ==  nil {
                    // value.removeValue(forKey: time)
                    // print("existo")
                }
                
            }
            
            // Set Attributes for Resolve Button
            
            //self.testFirebaseReturn =(snap.value as AnyObject).description
            self.resolveButton.center = self.DPoint (oldPoint: CGPoint(x: 350, y: 225))
            self.resolveButton.layer.cornerRadius = 10
            self.resolveButton.backgroundColor = UIColor(red: CGFloat(102.0/225.0), green: CGFloat(204.0/225.0), blue: CGFloat(0.0/225.0), alpha: 1.0)
            self.resolveButton.setTitle("Resolve", for: .normal)
            self.resolveButton.addTarget(self, action: #selector(self.resolveAction), for: .touchUpInside)
            
            // Center all labels
            
            
            self.name.center = self.DPoint (oldPoint: CGPoint(x: 210, y: 75))
            self.number.center = self.DPoint (oldPoint: CGPoint(x: 210, y: 125))
            self.desc.center = self.DPoint (oldPoint: CGPoint(x: 210, y: 225))
            self.loc.center = self.DPoint (oldPoint: CGPoint(x: 210, y: 175))
            
            
            // Set text for all Labels
            
            
            self.name.text = "      Time: "
            self.number.text = "     Name: "
            self.desc.text = "Location: "
            self.loc.text = " Problem: "
            
            
            // Clip all Labels to bounds
            
            self.name.clipsToBounds = true
            self.number.clipsToBounds = true
            self.desc.clipsToBounds = true
            self.loc.clipsToBounds = true
            
            // Set curved corners for all Labels
            
            
            self.loc.layer.cornerRadius = 5
            self.desc.layer.cornerRadius = 5
            self.number.layer.cornerRadius = 5
            self.name.layer.cornerRadius = 5
            
            // Add all labels to page
            
            
            self.view.addSubview(self.name)
            self.view.addSubview(self.number)
            self.view.addSubview(self.desc)
            self.view.addSubview(self.loc)
            self.view.addSubview(self.resolveButton)
            
            //Intialize and set attributes for TableView
            
            self.tableView = UITableView(frame: self.DFrame( oldFrame: CGRect(x: 0, y: 250, width: 410, height: 422)), style: UITableViewStyle.plain)
            self.tableView.center = self.DPoint(oldPoint: CGPoint(x: 208, y: 467))
            self.tableView.layer.cornerRadius = 10
            self.tableView.delegate      =   self
            self.tableView.dataSource    =   self
            self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
            let numberOfSections = self.tableView.numberOfSections
            let numberOfRows = self.tableView.numberOfRows(inSection: numberOfSections-1)
            let indexPath = IndexPath(row: numberOfRows-1 , section: numberOfSections-1)
            // self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.bottom, animated: true)
            //scrollView = UIScrollView(frame: view.bounds)
            //scrollView.backgroundColor = UIColor.black
            // scrollView.contentSize = imageView.bounds.size
            // scrollView.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
            
            // scrollView.addSubview(imageView)
            ///view.addSubview(scrollView)
            self.view.addSubview(self.tableView)
            // self.alert(dict: value)
            
        }
    }
    
    
    
        override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    }

