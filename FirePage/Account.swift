//
//  Account.swift
//  FirePage
//
//  Created by The Ritler on 1/22/17.
//  Copyright © 2017 The Ritler. All rights reserved.
//
import UIKit
import Firebase

class Account: UIViewController, UITextFieldDelegate {
    let master = UIApplication.shared.delegate as! AppDelegate
    
    let userRef = FIRDatabase.database().reference().child("users")
    
    let deviceUUID: String = (UIDevice.current.identifierForVendor?.uuidString)!
    
    var changePasswordButton = UIButton()
    
    var changeUserNameButton = UIButton()
    
    var changeNameButton = UIButton()
    
    var logOutButton = UIButton()
    
    var name = UIButton()
    
    var users = [String: [String : String]]()
    
    var popupPasswordElements = [UIView]()
    
    var popupNameElements = [UIView]()
    
    var popupNumberElements = [UIView]()
    
    let viewX = 414
    
    let viewY = 736
    
    var oldPasswordField = UITextField()
    
    var newNameField = UITextField()
    
    var newNumberField = UITextField()
    
    var newPasswordField = UITextField()
    
    let badCharSet = CharacterSet(charactersIn: ".$[]#/:()\\")
    let letterSet = CharacterSet(charactersIn:"zxcvbnmasdfghjklqwertyuiopZXCVBNMASDFGHJKLQWERTYUIOP")
    let numberSet = CharacterSet(charactersIn: "1234567890")
    let specialSet = CharacterSet(charactersIn: ",/;'|`-=<>?~!@%^&*+")
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("loo")
        textField.resignFirstResponder()
        return true;
    }
    func setX(x: Int) -> Int {
        return Int((Float(x)/414.0)*Float(UIScreen.main.bounds.width))
    }
    func setY(y: Int) -> Int {
        return Int((Float(y)/736.0)*Float(UIScreen.main.bounds.height))
    }
    func DFrame (oldFrame: CGRect) -> CGRect {
        let newX = Int((Float(oldFrame.minX)/414.0)*Float(UIScreen.main.bounds.width))
        let newY = Int((Float(oldFrame.minY)/736.0)*Float(UIScreen.main.bounds.height))
        let newW = Int((Float(oldFrame.width)/414.0)*Float(UIScreen.main.bounds.width))
        let newH = Int((Float(oldFrame.height)/736.0)*Float(UIScreen.main.bounds.height))
        
        return CGRect(x: newX, y: newY, width: newW, height: newH)
    }
    func DPoint (oldPoint: CGPoint) -> CGPoint {
        let newX = Int((Float(oldPoint.x)/414.0)*Float(UIScreen.main.bounds.width))
        let newY = Int((Float(oldPoint.y)/736.0)*Float(UIScreen.main.bounds.height))
        return CGPoint(x: newX, y: newY)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        userRef.observeSingleEvent(of: .value, with: { (snapshot) in
            
            
            self.users = (snapshot.value as! NSDictionary) as! [String : [String : String]]
            
            
            
        }) { (error) in
            print(error.localizedDescription)
        }
        
        createButtons()
        
        
        
    }
    func numberEnterAction(sendero: UIButton!) {
        let passwordContainsBad : Bool = newNumberField.text!.lowercased().rangeOfCharacter(from: badCharSet) != nil
        //let containsLetter : Bool = newNameField.text!.rangeOfCharacter(from: letterSet) != nil
        //let containsNumber : Bool = newNameField.text!.rangeOfCharacter(from: numberSet) != nil
        //let containsSpecial : Bool = newNameField.text!.rangeOfCharacter(from: specialSet) != nil
        var onlyNums: Bool = true
        for char in newNumberField.text!.characters {
            if String(char).rangeOfCharacter(from: numberSet) == nil{
                onlyNums = false
                
            }
        }
        if newNumberField.text! == ""{
            onlyNums = false
        }
        
        if !passwordContainsBad && onlyNums{
            if String(oldPasswordField.text!.hashValue) == users[master.userName]?["password"] {
                print(oldPasswordField.text)
                users[master.userName]?["number"] = String(newNumberField.text!)
                print(users[master.userName]?["number"])
                userRef.updateChildValues(users)
                clearNumberPopup()
            } else {
                print(String(oldPasswordField.text!.hashValue))
                print(users[master.userName]?["password"])
                oldPasswordField.text = ""
                oldPasswordField.attributedPlaceholder = NSAttributedString(string: "Wrong Password",
                                                                            attributes: [NSForegroundColorAttributeName: UIColor.red])
                
                
            }
        } else{
            newNumberField.text = ""
            newNumberField.attributedPlaceholder = NSAttributedString(string: "Try Again (See Rules)",
                                                                    attributes: [NSForegroundColorAttributeName: UIColor.red])
        }
    }
    
    func exitNumberButton(sendero: UIButton!) {
        clearNumberPopup()
    }
    
    func clearNumberPopup(){
        for element in popupNumberElements {
            element.removeFromSuperview()
            
        }
    }
    
    func createNumberPopup(){
        
        let PopupFrame = DFrame (oldFrame: CGRect(x: viewX/2 , y: viewY/2, width: viewX - 100, height: viewY - 200 ))
        
        
        
        
        let PopupX = UIButton(frame: DFrame(oldFrame: CGRect(x : 0, y: 0, width: 20, height: 20)))
        var Popup = UIView(frame: PopupFrame)
        Popup.center = DPoint (oldPoint: CGPoint(x: viewX/2 , y: viewY/2))
        PopupX.center = DPoint (oldPoint: CGPoint(x: viewX - 65, y : 115 ))
        
        PopupX.setImage(#imageLiteral(resourceName: "Close"), for: .normal)
        
        Popup.backgroundColor = .white
        
        
        oldPasswordField = UITextField(frame: CGRect(x: 20, y: 100, width: 260, height: 50))
        oldPasswordField.center = DPoint (oldPoint: CGPoint(x: viewX/2, y: 280))
        oldPasswordField.placeholder = "Enter Password"
        oldPasswordField.font = UIFont.systemFont(ofSize: 18)
        oldPasswordField.borderStyle = UITextBorderStyle.roundedRect
        oldPasswordField.autocorrectionType = UITextAutocorrectionType.no
        // locationField.backgroundColor = .red
        oldPasswordField.keyboardType = UIKeyboardType.default
        oldPasswordField.returnKeyType = UIReturnKeyType.done
        oldPasswordField.clearButtonMode = UITextFieldViewMode.whileEditing;
        oldPasswordField.contentVerticalAlignment =         UIControlContentVerticalAlignment.center
        oldPasswordField.autocapitalizationType = .none
        oldPasswordField.isSecureTextEntry = true
        oldPasswordField.delegate = self
        
        newNumberField = UITextField(frame: CGRect(x: 20, y: 100, width: 260, height: 50))
        newNumberField.center = DPoint (oldPoint: CGPoint(x: viewX/2, y: 340))
        newNumberField.placeholder = "Enter New Number"
        newNumberField.font = UIFont.systemFont(ofSize: 18)
        newNumberField.borderStyle = UITextBorderStyle.roundedRect
        newNumberField.autocorrectionType = UITextAutocorrectionType.no
        // locationFieldo.backgroundColor = .red
        newNumberField.keyboardType = UIKeyboardType.default
        newNumberField.returnKeyType = UIReturnKeyType.done
        newNumberField.clearButtonMode = UITextFieldViewMode.whileEditing;
        newNumberField.contentVerticalAlignment =         UIControlContentVerticalAlignment.center
        newNumberField.autocapitalizationType = .none
        // newNumberField.isSecureTextEntry = true
        newNumberField.delegate = self
        
        popupNumberElements.append(oldPasswordField)
        popupNumberElements.append(newNumberField)
        popupNumberElements.append(Popup)
        popupNumberElements.append(PopupX)
        Popup.layer.cornerRadius = 10
        Popup.layer.shadowRadius = 10.0;
        Popup.layer.shadowOpacity = 0.5;
        
        PopupX.addTarget(self, action: #selector(exitNumberButton), for: .touchUpInside)
        var enterButton = UIButton(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 100, height: 50)))
        enterButton.center = DPoint (oldPoint: CGPoint(x: viewX/2 , y: (viewY/2) + 130 ))
        enterButton.backgroundColor = UIColor(red: CGFloat(102.0/225.0), green: CGFloat(204.0/225.0), blue: CGFloat(0.0/225.0), alpha: 1.0)
        enterButton.setTitle("Enter", for: .normal)
        enterButton.layer.cornerRadius = 10
        enterButton.addTarget(self, action: #selector(numberEnterAction), for: .touchUpInside)
        
        popupNumberElements.append(enterButton)
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        //always fill the view
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        let nameInfo = UILabel(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 260, height: 48)))
        nameInfo.backgroundColor = .clear
        // nameInfo.layer.cornerRadius = 10
        let numberInfo = UILabel(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 260, height: 48)))
        numberInfo.backgroundColor = .clear
        numberInfo.layer.cornerRadius = 10
        let descInfo = UILabel(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 260, height: 48)))
        descInfo.backgroundColor = .clear
        descInfo.layer.cornerRadius = 10
        let locInfo = UILabel(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 260, height: 48)))
        locInfo.backgroundColor = .clear
        locInfo.layer.cornerRadius = 10
        nameInfo.center = self.DPoint (oldPoint: CGPoint(x: viewX/2, y: 160 ))
        numberInfo.center = self.DPoint (oldPoint: CGPoint(x: viewX/2, y: 210))
        descInfo.center = self.DPoint (oldPoint: CGPoint(x: viewX/2, y: 185))
        locInfo.center = self.DPoint (oldPoint: CGPoint(x: viewX/2, y: 310))
        nameInfo.attributedText = NSAttributedString(string: "Please enter valid number",
                                                     attributes: [NSForegroundColorAttributeName: UIColor.gray])
        nameInfo.textAlignment = .center
        nameInfo.font = UIFont.systemFont(ofSize: 12)
        numberInfo.attributedText = NSAttributedString(string: "Invalid special characters are .$[]#/:()\\",
                                                       attributes: [NSForegroundColorAttributeName: UIColor.gray])
        numberInfo.font = UIFont.systemFont(ofSize: 12)
        numberInfo.textAlignment = .center
        descInfo.attributedText = NSAttributedString(string: "No dashes only digits",
                                                     attributes: [NSForegroundColorAttributeName: UIColor.gray])
        descInfo.textAlignment = .center
        descInfo.font = UIFont.systemFont(ofSize: 12)
        // locInfo.text = loc.text?.trim()
        nameInfo.clipsToBounds = true
        numberInfo.clipsToBounds = true
        descInfo.clipsToBounds = true
        locInfo.clipsToBounds = true
        nameInfo.layer.cornerRadius = 5
        numberInfo.layer.cornerRadius = 5
        descInfo.layer.cornerRadius = 5
        locInfo.layer.cornerRadius = 5
        popupNumberElements.append(nameInfo)
        popupNumberElements.append(numberInfo)
        popupNumberElements.append(descInfo)
        popupNumberElements.append(locInfo)
        popupNumberElements.append(blurEffectView)
        self.view.addSubview(blurEffectView)
        self.view.addSubview(PopupX)
        self.view.addSubview(Popup)
        self.view.bringSubview(toFront: PopupX)
        self.view.addSubview(enterButton)
        self.view.bringSubview(toFront: enterButton)
        self.view.addSubview(oldPasswordField)
        self.view.addSubview(newNumberField)
        self.view.addSubview(nameInfo)
        self.view.addSubview(numberInfo)
        self.view.addSubview(descInfo)
        // self.view.addSubview(locInfo)
        
        
        
        
    }
    func nameEnterAction(sendero: UIButton!) {
        let passwordContainsBad : Bool = newNameField.text!.lowercased().rangeOfCharacter(from: badCharSet) != nil
        //let containsLetter : Bool = newNameField.text!.rangeOfCharacter(from: letterSet) != nil
        //let containsNumber : Bool = newNameField.text!.rangeOfCharacter(from: numberSet) != nil
        //let containsSpecial : Bool = newNameField.text!.rangeOfCharacter(from: specialSet) != nil
        
        
        if !passwordContainsBad{
            if String(oldPasswordField.text!.hashValue) == users[master.userName]?["password"] {
                print(oldPasswordField.text)
                users[master.userName]?["name"] = String(newNameField.text!)
                print(users[master.userName]?["name"])
                userRef.updateChildValues(users)
                name.setTitle(newNameField.text!, for: .normal)
                master.name = newNameField.text!
                clearNamePopup()
            } else {
                print(String(oldPasswordField.text!.hashValue))
                print(users[master.userName]?["password"])
                oldPasswordField.text = ""
                oldPasswordField.attributedPlaceholder = NSAttributedString(string: "Wrong Password",
                                                                            attributes: [NSForegroundColorAttributeName: UIColor.red])
                
                
            }
        } else{
            newNameField.text = ""
            newNameField.attributedPlaceholder = NSAttributedString(string: "Try Again (See Rules)",
                                                                        attributes: [NSForegroundColorAttributeName: UIColor.red])
        }
    }
    
    func exitNameButton(sendero: UIButton!) {
        clearNamePopup()
    }
    
    func clearNamePopup(){
        for element in popupNameElements {
            element.removeFromSuperview()
            
        }
    }
    
    func createNamePopup(){
        
        let PopupFrame = DFrame (oldFrame: CGRect(x: viewX/2 , y: viewY/2, width: viewX - 100, height: viewY - 200 ))
        
        
        
        
        let PopupX = UIButton(frame: DFrame(oldFrame: CGRect(x : 0, y: 0, width: 20, height: 20)))
        var Popup = UIView(frame: PopupFrame)
        Popup.center = DPoint (oldPoint: CGPoint(x: viewX/2 , y: viewY/2))
        PopupX.center = DPoint (oldPoint: CGPoint(x: viewX - 65, y : 115 ))
        
        PopupX.setImage(#imageLiteral(resourceName: "Close"), for: .normal)
        
        Popup.backgroundColor = .white
        
        
        oldPasswordField = UITextField(frame: CGRect(x: 20, y: 100, width: 260, height: 50))
        oldPasswordField.center = DPoint (oldPoint: CGPoint(x: viewX/2, y: 280))
        oldPasswordField.placeholder = "Enter Password"
        oldPasswordField.font = UIFont.systemFont(ofSize: 18)
        oldPasswordField.borderStyle = UITextBorderStyle.roundedRect
        oldPasswordField.autocorrectionType = UITextAutocorrectionType.no
        // locationField.backgroundColor = .red
        oldPasswordField.keyboardType = UIKeyboardType.default
        oldPasswordField.returnKeyType = UIReturnKeyType.done
        oldPasswordField.clearButtonMode = UITextFieldViewMode.whileEditing;
        oldPasswordField.contentVerticalAlignment =         UIControlContentVerticalAlignment.center
        oldPasswordField.autocapitalizationType = .none
        oldPasswordField.isSecureTextEntry = true
        oldPasswordField.delegate = self
        
        newNameField = UITextField(frame: CGRect(x: 20, y: 100, width: 260, height: 50))
        newNameField.center = DPoint (oldPoint: CGPoint(x: viewX/2, y: 340))
        newNameField.placeholder = "Enter New Name"
        newNameField.font = UIFont.systemFont(ofSize: 18)
        newNameField.borderStyle = UITextBorderStyle.roundedRect
        newNameField.autocorrectionType = UITextAutocorrectionType.no
        // locationFieldo.backgroundColor = .red
        newNameField.keyboardType = UIKeyboardType.default
        newNameField.returnKeyType = UIReturnKeyType.done
        newNameField.clearButtonMode = UITextFieldViewMode.whileEditing;
        newNameField.contentVerticalAlignment =         UIControlContentVerticalAlignment.center
        newNameField.autocapitalizationType = .none
        // newNameField.isSecureTextEntry = true
        newNameField.delegate = self
        
        popupNameElements.append(oldPasswordField)
        popupNameElements.append(newNameField)
        popupNameElements.append(Popup)
        popupNameElements.append(PopupX)
        Popup.layer.cornerRadius = 10
        Popup.layer.shadowRadius = 10.0;
        Popup.layer.shadowOpacity = 0.5;
        
        PopupX.addTarget(self, action: #selector(exitNameButton), for: .touchUpInside)
        var enterButton = UIButton(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 100, height: 50)))
        enterButton.center = DPoint (oldPoint: CGPoint(x: viewX/2 , y: (viewY/2) + 130 ))
        enterButton.backgroundColor = UIColor(red: CGFloat(102.0/225.0), green: CGFloat(204.0/225.0), blue: CGFloat(0.0/225.0), alpha: 1.0)
        enterButton.setTitle("Enter", for: .normal)
        enterButton.layer.cornerRadius = 10
        enterButton.addTarget(self, action: #selector(nameEnterAction), for: .touchUpInside)
        
        popupNameElements.append(enterButton)
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        //always fill the view
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        let nameInfo = UILabel(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 260, height: 48)))
        nameInfo.backgroundColor = .clear
        // nameInfo.layer.cornerRadius = 10
        let numberInfo = UILabel(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 260, height: 48)))
        numberInfo.backgroundColor = .clear
        numberInfo.layer.cornerRadius = 10
        let descInfo = UILabel(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 260, height: 48)))
        descInfo.backgroundColor = .clear
        descInfo.layer.cornerRadius = 10
        let locInfo = UILabel(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 260, height: 48)))
        locInfo.backgroundColor = .clear
        locInfo.layer.cornerRadius = 10
        nameInfo.center = self.DPoint (oldPoint: CGPoint(x: viewX/2, y: 160 ))
        numberInfo.center = self.DPoint (oldPoint: CGPoint(x: viewX/2, y: 210))
        descInfo.center = self.DPoint (oldPoint: CGPoint(x: viewX/2, y: 185))
        locInfo.center = self.DPoint (oldPoint: CGPoint(x: viewX/2, y: 310))
        nameInfo.attributedText = NSAttributedString(string: "Please use First and Last Name",
                                                     attributes: [NSForegroundColorAttributeName: UIColor.gray])
        nameInfo.textAlignment = .center
        nameInfo.font = UIFont.systemFont(ofSize: 12)
        numberInfo.attributedText = NSAttributedString(string: "Invalid special characters are .$[]#/:()\\",
                                                       attributes: [NSForegroundColorAttributeName: UIColor.gray])
        numberInfo.font = UIFont.systemFont(ofSize: 12)
        numberInfo.textAlignment = .center
        descInfo.attributedText = NSAttributedString(string: "Do not use invalid special characters",
                                                     attributes: [NSForegroundColorAttributeName: UIColor.gray])
        descInfo.textAlignment = .center
        descInfo.font = UIFont.systemFont(ofSize: 12)
        // locInfo.text = loc.text?.trim()
        nameInfo.clipsToBounds = true
        numberInfo.clipsToBounds = true
        descInfo.clipsToBounds = true
        locInfo.clipsToBounds = true
        nameInfo.layer.cornerRadius = 5
        numberInfo.layer.cornerRadius = 5
        descInfo.layer.cornerRadius = 5
        locInfo.layer.cornerRadius = 5
        popupNameElements.append(nameInfo)
        popupNameElements.append(numberInfo)
        popupNameElements.append(descInfo)
        popupNameElements.append(locInfo)
        popupNameElements.append(blurEffectView)
        self.view.addSubview(blurEffectView)
        self.view.addSubview(PopupX)
        self.view.addSubview(Popup)
        self.view.bringSubview(toFront: PopupX)
        self.view.addSubview(enterButton)
        self.view.bringSubview(toFront: enterButton)
        self.view.addSubview(oldPasswordField)
        self.view.addSubview(newNameField)
        self.view.addSubview(nameInfo)
        self.view.addSubview(numberInfo)
        self.view.addSubview(descInfo)
        // self.view.addSubview(locInfo)
        
        
        
        
    }
    func changePassword(sendero: UIButton!) {
        
        createPasswordPopup()
    }
    func changeUserName(sendero: UIButton!) {
        createNumberPopup()
    }
    func changeName(sendero: UIButton!) {
        createNamePopup()
    }
    func logOut(sendero: UIButton!) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController : UIViewController = storyBoard.instantiateViewController(withIdentifier: "Login")
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    func createButtons(){
        name = UIButton(frame: DFrame(oldFrame: CGRect(x: 0, y: 20, width: 430, height: 50)))
        name.backgroundColor = UIColor(red: CGFloat(0.0/225.0), green: CGFloat(0.0/225.0), blue: CGFloat(220.0/225.0), alpha: 0.65)
        name.center = DPoint(oldPoint : CGPoint(x: 207, y: 50))
        let nametexto : String = "Hi! " + master.name
        name.setTitle(nametexto, for: .normal)
        name.titleLabel!.textAlignment = .center
        name.layer.cornerRadius = 5
        name.setTitleColor(.white , for: .normal)
        self.view.addSubview(name)
        changePasswordButton = UIButton(frame: DFrame(oldFrame: CGRect(x: 300, y: 300, width: 300, height: 50)))
        
        changePasswordButton.backgroundColor = UIColor(red: CGFloat(59.0/225.0), green: CGFloat(108.0/225.0), blue: CGFloat(150.0/225.0), alpha: 0.9)
        
        changePasswordButton.center = DPoint(oldPoint : CGPoint(x: 210, y: 320))
        changePasswordButton.setTitle("Change Password", for: .normal)
        changePasswordButton.addTarget(self, action: #selector(changePassword), for: .touchUpInside)
        changePasswordButton.titleLabel!.textAlignment = .center
        
        changePasswordButton.layer.cornerRadius = 5
        changePasswordButton.setTitleColor(.white , for: .normal)
        changeUserNameButton = UIButton(frame: DFrame(oldFrame: CGRect(x: 300, y: 300, width: 300, height: 50)))
        
        changeUserNameButton.backgroundColor = UIColor(red: CGFloat(84.0/225.0), green: CGFloat(156.0/225.0), blue: CGFloat(216.0/225.0), alpha: 0.9)
        
        changeUserNameButton.center = DPoint(oldPoint : CGPoint(x: 210, y: 200))
        changeUserNameButton.setTitle("Change Phone Number", for: .normal)
        changeUserNameButton.addTarget(self, action: #selector(changeUserName), for: .touchUpInside)
        changeUserNameButton.titleLabel!.textAlignment = .center
        changeUserNameButton.layer.cornerRadius = 5
        changeUserNameButton.setTitleColor(.white , for: .normal)
        
        changeNameButton = UIButton(frame: DFrame(oldFrame: CGRect(x: 300, y: 300, width: 300, height: 50)))
        
        changeNameButton.backgroundColor = UIColor(red: CGFloat(70.0/225.0), green: CGFloat(130.0/225.0), blue: CGFloat(180.0/225.0), alpha: 0.9)
        
        changeNameButton.center = DPoint(oldPoint : CGPoint(x: 210, y: 260))
        changeNameButton.setTitle("Change Name", for: .normal)
        changeNameButton.addTarget(self, action: #selector(changeName), for: .touchUpInside)
        changeNameButton.titleLabel!.textAlignment = .center
        
        changeNameButton.layer.cornerRadius = 5
        changeNameButton.setTitleColor(.white , for: .normal)
        
        logOutButton = UIButton(frame: DFrame(oldFrame: CGRect(x: 300, y: 300, width: 300, height: 50)))
        
        logOutButton.backgroundColor = UIColor(red: CGFloat(128.0/225.0), green: CGFloat(0.0/225.0), blue: CGFloat(0.0/225.0), alpha: 0.9)
        
        logOutButton.center = DPoint(oldPoint : CGPoint(x: 210, y: 400))
        logOutButton.setTitle("Log Out", for: .normal)
        logOutButton.addTarget(self, action: #selector(logOut), for: .touchUpInside)
        logOutButton.titleLabel!.textAlignment = .center
        
        logOutButton.layer.cornerRadius = 5
        logOutButton.setTitleColor(.white , for: .normal)
        
        self.view.addSubview(changePasswordButton)
        self.view.addSubview(changeNameButton)
        self.view.addSubview(changeUserNameButton)
        self.view.addSubview(logOutButton)
        
        
        
    }
    func passwordEnterAction(sendero: UIButton!) {
        let passwordContainsBad : Bool = newPasswordField.text!.lowercased().rangeOfCharacter(from: badCharSet) != nil
        let containsLetter : Bool = newPasswordField.text!.rangeOfCharacter(from: letterSet) != nil
        let containsNumber : Bool = newPasswordField.text!.rangeOfCharacter(from: numberSet) != nil
        let containsSpecial : Bool = newPasswordField.text!.rangeOfCharacter(from: specialSet) != nil
        
        
        if !passwordContainsBad && containsLetter && containsNumber && containsSpecial{
        if String(oldPasswordField.text!.hashValue) == users[master.userName]?["password"] {
            print(oldPasswordField.text)
            users[master.userName]?["password"] = String(newPasswordField.text!.hashValue)
            print(users[master.userName]?["password"])
            userRef.updateChildValues(users)
            oldPasswordField.text = newPasswordField.text
            clearPasswordPopup()
        } else {
            print(String(oldPasswordField.text!.hashValue))
            print(users[master.userName]?["password"])
            oldPasswordField.text = ""
            oldPasswordField.attributedPlaceholder = NSAttributedString(string: "Wrong Old Password",
                                                                        attributes: [NSForegroundColorAttributeName: UIColor.red])
            
            
        }
        } else{
            newPasswordField.text = ""
            newPasswordField.attributedPlaceholder = NSAttributedString(string: "Try Again (See Rules)",
                                                                        attributes: [NSForegroundColorAttributeName: UIColor.red])
        }
    }
    
    func exitPasswordButton(sendero: UIButton!) {
        clearPasswordPopup()
    }
    
    func clearPasswordPopup(){
        for element in popupPasswordElements {
            element.removeFromSuperview()
            
        }
    }
    
    func createPasswordPopup(){
        
        let PopupFrame = DFrame (oldFrame: CGRect(x: viewX/2 , y: viewY/2, width: viewX - 100, height: viewY - 200 ))
        
        
        
        
        let PopupX = UIButton(frame: DFrame(oldFrame: CGRect(x : 0, y: 0, width: 20, height: 20)))
        var Popup = UIView(frame: PopupFrame)
        Popup.center = DPoint (oldPoint: CGPoint(x: viewX/2 , y: viewY/2))
        PopupX.center = DPoint (oldPoint: CGPoint(x: viewX - 65, y : 115 ))
        
        PopupX.setImage(#imageLiteral(resourceName: "Close"), for: .normal)
        
        Popup.backgroundColor = .white
        
        
        oldPasswordField = UITextField(frame: CGRect(x: 20, y: 100, width: 260, height: 50))
        oldPasswordField.center = DPoint (oldPoint: CGPoint(x: viewX/2, y: 280))
        oldPasswordField.placeholder = "Enter Old Password"
        oldPasswordField.font = UIFont.systemFont(ofSize: 18)
        oldPasswordField.borderStyle = UITextBorderStyle.roundedRect
        oldPasswordField.autocorrectionType = UITextAutocorrectionType.no
        // locationField.backgroundColor = .red
        oldPasswordField.keyboardType = UIKeyboardType.default
        oldPasswordField.returnKeyType = UIReturnKeyType.done
        oldPasswordField.clearButtonMode = UITextFieldViewMode.whileEditing;
        oldPasswordField.contentVerticalAlignment =         UIControlContentVerticalAlignment.center
        oldPasswordField.autocapitalizationType = .none
        oldPasswordField.isSecureTextEntry = true
        oldPasswordField.delegate = self
        
        newPasswordField = UITextField(frame: CGRect(x: 20, y: 100, width: 260, height: 50))
        newPasswordField.center = DPoint (oldPoint: CGPoint(x: viewX/2, y: 340))
        newPasswordField.placeholder = "Enter New Password"
        newPasswordField.font = UIFont.systemFont(ofSize: 18)
        newPasswordField.borderStyle = UITextBorderStyle.roundedRect
        newPasswordField.autocorrectionType = UITextAutocorrectionType.no
        // locationFieldo.backgroundColor = .red
        newPasswordField.keyboardType = UIKeyboardType.default
        newPasswordField.returnKeyType = UIReturnKeyType.done
        newPasswordField.clearButtonMode = UITextFieldViewMode.whileEditing;
        newPasswordField.contentVerticalAlignment =         UIControlContentVerticalAlignment.center
        newPasswordField.autocapitalizationType = .none
        newPasswordField.isSecureTextEntry = true
        newPasswordField.delegate = self
        
        popupPasswordElements.append(oldPasswordField)
        popupPasswordElements.append(newPasswordField)
        popupPasswordElements.append(Popup)
        popupPasswordElements.append(PopupX)
        Popup.layer.cornerRadius = 10
        Popup.layer.shadowRadius = 10.0;
        Popup.layer.shadowOpacity = 0.5;
        
        PopupX.addTarget(self, action: #selector(exitPasswordButton), for: .touchUpInside)
        var enterButton = UIButton(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 100, height: 50)))
        enterButton.center = DPoint (oldPoint: CGPoint(x: viewX/2 , y: (viewY/2) + 130 ))
        enterButton.backgroundColor = UIColor(red: CGFloat(102.0/225.0), green: CGFloat(204.0/225.0), blue: CGFloat(0.0/225.0), alpha: 1.0)
        enterButton.setTitle("Enter", for: .normal)
        enterButton.layer.cornerRadius = 10
        enterButton.addTarget(self, action: #selector(passwordEnterAction), for: .touchUpInside)
        
        popupPasswordElements.append(enterButton)
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        //always fill the view
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        let nameInfo = UILabel(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 260, height: 48)))
        nameInfo.backgroundColor = .clear
        // nameInfo.layer.cornerRadius = 10
        let numberInfo = UILabel(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 260, height: 48)))
        numberInfo.backgroundColor = .clear
        numberInfo.layer.cornerRadius = 10
        let descInfo = UILabel(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 260, height: 48)))
        descInfo.backgroundColor = .clear
        descInfo.layer.cornerRadius = 10
        let locInfo = UILabel(frame: DFrame (oldFrame: CGRect(x: 0, y: 200, width: 260, height: 48)))
        locInfo.backgroundColor = .clear
        locInfo.layer.cornerRadius = 10
        nameInfo.center = self.DPoint (oldPoint: CGPoint(x: viewX/2, y: 160 ))
        numberInfo.center = self.DPoint (oldPoint: CGPoint(x: viewX/2, y: 210))
        descInfo.center = self.DPoint (oldPoint: CGPoint(x: viewX/2, y: 185))
        locInfo.center = self.DPoint (oldPoint: CGPoint(x: viewX/2, y: 310))
        nameInfo.attributedText = NSAttributedString(string: "Password must contain letters numbers",
                           attributes: [NSForegroundColorAttributeName: UIColor.gray])
        nameInfo.textAlignment = .center
        nameInfo.font = UIFont.systemFont(ofSize: 12)
        numberInfo.attributedText = NSAttributedString(string: "Invalid special characters are .$[]#/:()\\",
                           attributes: [NSForegroundColorAttributeName: UIColor.gray])
        numberInfo.font = UIFont.systemFont(ofSize: 12)
        numberInfo.textAlignment = .center
        descInfo.attributedText = NSAttributedString(string: "and valid special characters",
                           attributes: [NSForegroundColorAttributeName: UIColor.gray])
        descInfo.textAlignment = .center
        descInfo.font = UIFont.systemFont(ofSize: 12)
        // locInfo.text = loc.text?.trim()
        nameInfo.clipsToBounds = true
        numberInfo.clipsToBounds = true
        descInfo.clipsToBounds = true
        locInfo.clipsToBounds = true
        nameInfo.layer.cornerRadius = 5
        numberInfo.layer.cornerRadius = 5
        descInfo.layer.cornerRadius = 5
        locInfo.layer.cornerRadius = 5
        popupPasswordElements.append(nameInfo)
        popupPasswordElements.append(numberInfo)
        popupPasswordElements.append(descInfo)
        popupPasswordElements.append(locInfo)
        popupPasswordElements.append(blurEffectView)
        self.view.addSubview(blurEffectView)
        self.view.addSubview(PopupX)
        self.view.addSubview(Popup)
        self.view.bringSubview(toFront: PopupX)
        self.view.addSubview(enterButton)
        self.view.bringSubview(toFront: enterButton)
        self.view.addSubview(oldPasswordField)
        self.view.addSubview(newPasswordField)
        self.view.addSubview(nameInfo)
        self.view.addSubview(numberInfo)
        self.view.addSubview(descInfo)
        // self.view.addSubview(locInfo)
        
        
        
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
   
}
