//
//  TabBar.swift
//  FirePage
//
//  Created by The Ritler on 12/30/16.
//  Copyright © 2016 The Ritler. All rights reserved.
//
// import FireBase
import Foundation
import UIKit
import Firebase

class myTabBar: UITabBarController, UITabBarControllerDelegate {
    let master = UIApplication.shared.delegate as! AppDelegate
    let pageRef = FIRDatabase.database().reference().child("pages").child("Ritwik")
    var date = NSDate()
    var calendar = NSCalendar.current
    var month = 1
    var year = 1
    var day = 1
    var hour = 1
    var minute = 1
    var second = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        
        self.calendar = NSCalendar.current
        self.month = calendar.component(.month, from: self.date as Date)
        self.year = calendar.component(.year, from: self.date as Date)
        self.day = calendar.component(.day, from: self.date as Date)
        self.hour = calendar.component(.hour, from: self.date as Date)
        self.minute = calendar.component(.minute, from: self.date as Date)
        self.second = calendar.component(.second, from: self.date as Date)
        var month = String(format: "%02d", self.month)
        var day = String(format: "%02d", self.day)
        var year = String(format: "%04d", self.year)
        var hour = String(format: "%02d", self.hour)
        var minute = String(format: "%02d", self.minute)
        var second = String(format: "%02d", self.second)
        
        
        let login = UIButton(frame: DFrame(oldFrame: CGRect(x: 0, y: 20, width: 200, height: 50)))
        
        login.backgroundColor = UIColor(red: CGFloat(0.0/225.0), green: CGFloat(0.0/225.0), blue: CGFloat(220.0/225.0), alpha: 0.65)
        
        // login.center = DPoint(oldPoint : CGPoint(x: 210, y: 300))
        login.setTitle(master.name, for: .normal)
        
        login.titleLabel!.textAlignment = .center
        
        login.layer.cornerRadius = 5
        login.setTitleColor(.white , for: .normal)
        // self.view.addSubview(login)
        
        
        
        let Contact = Page()
        let Calendar = ViewController()
        let MyPages = ResolvePages()
        let Accounto = Account()
        let SuperPago = SuperPage()
        
        let icon1 = UITabBarItem()
        icon1.title = "Contact"
        icon1.image = #imageLiteral(resourceName: "Contact")
        let icon2 = UITabBarItem()
        icon2.title = "Calendar"
        icon2.image = #imageLiteral(resourceName: "Calendar")
        let icon3 = UITabBarItem()
        icon3.title = "Resolve"
        icon3.image = #imageLiteral(resourceName: "Resolve")
        let icon4 = UITabBarItem()
        icon4.title = "Account"
        icon4.image = #imageLiteral(resourceName: "Settings")
        let icon5 = UITabBarItem()
        icon5.title = "Pages"
        icon5.image = #imageLiteral(resourceName: "Pages")
        
        
        
        
        Contact.tabBarItem = icon1
        Calendar.tabBarItem = icon2
        MyPages.tabBarItem = icon3
        Accounto.tabBarItem = icon4
        SuperPago.tabBarItem = icon5
        pageRef.observe(.value) { (snapshot: FIRDataSnapshot ) in
            var value : [String: [String : String]] = (snapshot.value as! NSDictionary) as! [String : [String : String]]
            // print(value)
            
            var unresolved = Int()
            unresolved = 0
            for (time, data) in value {
                var dateComponents : [String] = time.components(separatedBy: "|")
                var monthText : String! = dateComponents[0]
                var dayText : String! = dateComponents[1]
                var yearText : String! = dateComponents[2]
                var hourText : String! = dateComponents[3]
                var minuteText : String! = dateComponents[4]
                if data["status"] == "unresolved" && monthText == month && yearText == year && dayText == day{
                    unresolved = unresolved + 1
                }
                
            }
            if(unresolved != 0){
                MyPages.tabBarItem.badgeColor = .red
                MyPages.tabBarItem.badgeValue = String(unresolved)
            }else{
                MyPages.tabBarItem.badgeColor = .clear
                MyPages.tabBarItem.badgeValue = ""
                
            }
            
            
            
            
        }
        var controllers = [UIViewController]()
        if master.level == "employee"{
            controllers = [Contact, Calendar, MyPages, Accounto]
            
        } else if master.level == "admin" {
           controllers = [Contact, Calendar, SuperPago, Accounto]
            
        } else {
            controllers = [Contact, Accounto]
            
        }
        // print(master.level)
        self.viewControllers = controllers
    }
    func DFrame (oldFrame: CGRect) -> CGRect {
        let newX = Int((Float(oldFrame.minX)/414.0)*Float(UIScreen.main.bounds.width))
        let newY = Int((Float(oldFrame.minY)/736.0)*Float(UIScreen.main.bounds.height))
        let newW = Int((Float(oldFrame.width)/414.0)*Float(UIScreen.main.bounds.width))
        let newH = Int((Float(oldFrame.height)/736.0)*Float(UIScreen.main.bounds.height))
        
        return CGRect(x: newX, y: newY, width: newW, height: newH)
    }
    func DPoint (oldPoint: CGPoint) -> CGPoint {
        let newX = Int((Float(oldPoint.x)/414.0)*Float(UIScreen.main.bounds.width))
        let newY = Int((Float(oldPoint.y)/736.0)*Float(UIScreen.main.bounds.height))
        return CGPoint(x: newX, y: newY)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    //Delegate methods
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        // print("Should select viewController: \(viewController.title) ?")
        return true;
    }
}
